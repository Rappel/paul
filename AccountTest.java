package jforex;

import java.util.*;

import com.dukascopy.api.*;

public class AccountTest implements IStrategy {
    private IEngine engine;
    private IConsole console;
    private IHistory history;
    private IContext context;
    private IAccount account;
    private IIndicators indicators;
    private IUserInterface userInterface;
    
    public void onStart(IContext context) throws JFException {
        this.engine = context.getEngine();
        this.console = context.getConsole();
        this.history = context.getHistory();
        this.context = context;
        this.indicators = context.getIndicators();
        this.userInterface = context.getUserInterface();
        
        this.account = context.getAccount();
        
        console.getOut().println("Account Currency " + account.getAccountCurrency() );
        console.getOut().println("Equity " + account.getEquity() );
        console.getOut().println("StopLossLevel " + account.getStopLossLevel() );
        console.getOut().println("Leverage " + account.getLeverage() );
        //console.getOut().println("" +  );
          
    }

    public void onAccount(IAccount account) throws JFException {
    }

    public void onMessage(IMessage message) throws JFException {
    }

    public void onStop() throws JFException {
    }

    public void onTick(Instrument instrument, ITick tick) throws JFException {
    }
    
    public void onBar(Instrument instrument, Period period, IBar askBar, IBar bidBar) throws JFException {
    }
}